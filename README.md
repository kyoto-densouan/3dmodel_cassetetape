# README #

1/3スケールのカセットテープ風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。 
元ファイルはAUTODESK 123D DESIGNです。 

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_cassetetape/raw/0f11a7bb05d2a675f523c1bbe2cf0451072834d6/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_cassetetape/raw/0f11a7bb05d2a675f523c1bbe2cf0451072834d6/ExampleImage.png)
